
setenv NWCHEM_TOP /gpfs/hpchome/*PATH*
setenv NWCHEM_TARGET LINUX64
setenv NWCHEM_MODULES "all"
setenv NWCHEM_MPIF_WRAP /storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/bin/mpiifort
setenv NWCHEM_MPIC_WRAP /storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/bin/mpiicc
setenv NWCHEM_MPICXX_WRAP /storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/bin/mpiicpc
setenv NWCHEM_LONG_PATHS Y
setenv USE_NOFSCHECK Y
setenv USE_MPI y
setenv USE_MPIF y
setenv USE_MPIF4 y
setenv MPI_INCLUDE "/storage/software/intel_parallel_studio_xe_2015//impi/5.0.1.035/intel64/include"
setenv MPI_LIB "/storage/software/intel_parallel_studio_xe_2015//impi/5.0.1.035/intel64/lib"
setenv LIBMPI "-lmpifort -lmpi -lmpigi -ldl -lrt -lpthread"
setenv FC ifort
setenv CC icc
setenv CXX icpc
setenv ARMCI_NETWORK MPI_TS
setenv MSG_COMMS MPI
setenv SLURMOPT -lslurm
setenv SLURM y
setenv PYTHON_EXE /storage/software/python-2.7.3/bin/python
setenv PYTHONVERSION 2.7.3
setenv USE_PYTHON64 yes
setenv PYTHONPATH ./:/gpfs/hpchome/truesigh/project/Nwchem-6.5/contrib/python/
setenv PYTHONHOME /storage/software/python-2.7.3/
setenv PYTHONLIBTYPE so
alias renwc 'make FC=$FC ; pushd $NWCHEM_TOP/src ; make FC=$FC link ; popd'
