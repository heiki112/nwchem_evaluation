If terminal says
```
#!bash

(gnome-ssh-askpass:18142): Gtk-WARNING **: cannot open display:
```
you need to do **unset SSH_ASKPASS** on rocket. This might happen when trying to clone the project or push changes.


Compiled NWChem is located in /bin folder. There are 3 benchmarks ready to run: **c240.nw**, **cyto.nw** (Cytosine-OH) and **b3lyp.nw**. Change the file name (and number of nodes, 1 for b3lyp) in rocketsubscript accordingly.


Joosep Kibal, Heiki Pärn