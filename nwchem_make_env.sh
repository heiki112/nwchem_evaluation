
export NWCHEM_TOP=/gpfs/hpchome/*PATH*
export NWCHEM_TARGET=LINUX64
export NWCHEM_MODULES="all"
export NWCHEM_MPIF_WRAP=/storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/bin/mpiifort
export NWCHEM_MPIC_WRAP=/storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/bin/mpiicc
export NWCHEM_MPICXX_WRAP=/storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/bin/mpiicpc
export NWCHEM_LONG_PATHS=Y
export USE_NOFSCHECK=Y
export USE_MPI=y
export USE_MPIF=y
export USE_MPIF4=y
export MPI_INCLUDE="/storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/include"
export MPI_LIB="/storage/software/intel_parallel_studio_xe_2015/impi/5.0.1.035/intel64/lib"
export LIBMPI="-lmpifort -lmpi -lmpigi -ldl -lrt -lpthread"
export FC=ifort
export CC=icc
export CXX=icpc
export ARMCI_NETWORK=MPI_TS
export MSG_COMMS=MPI
export SLURMOPT=-lslurm
export SLURM=y
export PYTHON_EXE=/storage/software/python-2.7.3/bin/python
export PYTHONVERSION=2.7.3
export USE_PYTHON64=yes
export PYTHONPATH=./:/gpfs/hpchome/truesigh/project/Nwchem-6.5/contrib/python/
export PYTHONHOME=/storage/software/python-2.7.3/
export PYTHONLIBTYPE=so
export FOPTIMIZE="-O3 -xSSE2,SSE3,SSSE3,SSE4.1,SSE4.2 -no-prec-div -funroll-loops -unroll-aggressive"
export COPTIMIZE="-O3 -xSSE2,SSE3,SSSE3,SSE4.1,SSE4.2 -no-prec-div -funroll-loops"
function renwc()
{
   make FC=$FC ; pushd $NWCHEM_TOP/src ; make FC=$FC link ; popd
}
