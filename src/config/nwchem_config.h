# This configuration generated automatically on rocket at Mon Nov 24 16:27:11 EET 2014
# Request modules from user: all
NW_MODULE_SUBDIRS = NWints atomscf ddscf gradients moints nwdft nwxc rimp2 stepper driver optim cphf ccsd vib mcscf prepar esp hessian selci dplot mp2_grad qhop property solvation nwpw fft analyz nwmd cafe space drdy vscf qmmm qmd etrans tce bq cons perfm dntmc dangchang ccca
NW_MODULE_LIBS = -lccsd -lmcscf -lselci -lmp2 -lmoints -lstepper -ldriver -loptim -lnwdft -lgradients -lcphf -lesp -lddscf -ldangchang -lguess -lhessian -lvib -lnwcutil -lrimp2 -lproperty -lsolvation -lnwints -lprepar -lnwmd -lnwpw -lofpw -lpaw -lpspw -lband -lnwpwlib -lnwxc -lcafe -lspace -lanalyze -lqhop -lpfft -ldplot -ldrdy -lvscf -lqmmm -lqmd -letrans -lpspw -ltce -lbq -lcons -lperfm -ldntmc -lccca
EXCLUDED_SUBDIRS = develop scfaux rimp2_grad python argos diana uccsdt rism geninterface smd nbo leps lucia
CONFIG_LIBS = 
